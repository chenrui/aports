# Maintainer: Ariadne Conill <ariadne@dereferenced.org>
pkgname=apko
pkgver=0.1.1
pkgrel=0
pkgdesc="declarative APK-based container building tool with support for Sigstore signatures"
url="https://github.com/chainguard-dev/apko"
arch="all"
license="Apache-2.0"
# Explicitly depend on apk-tools, so that this package can be used without alpine-base.
depends="apk-tools"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/chainguard-dev/apko/archive/v$pkgver/apko-$pkgver.tar.gz"

build() {
	mkdir build
	go build -o build/ -tags -tags=pivkey,pkcs11key "$builddir"/...

	for i in bash fish zsh; do
		"$builddir"/build/apko completion $i > "$builddir"/apko.$i
	done
}

check() {
	go test "$builddir"/...
}

package() {
	install -Dm755 "$builddir"/build/apko "$pkgdir"/usr/bin/apko

	install -Dm644 "$builddir"/apko.bash "$pkgdir"/usr/share/bash-completion/completions/apko
	install -Dm644 "$builddir"/apko.fish "$pkgdir"/usr/share/fish/completions/apko.fish
	install -Dm644 "$builddir"/apko.zsh "$pkgdir"/usr/share/zsh/site-functions/_apko
}

sha512sums="
db4fa340e6d80c5ff356c8d1800266354478582ac3e920296710de46ea7b59ff53bd0667081ee6757c0e50918f811097947960b6612d9fb9358246bc08ddafcd  apko-0.1.1.tar.gz
"
