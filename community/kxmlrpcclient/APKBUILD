# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kxmlrpcclient
pkgver=5.91.0
pkgrel=0
pkgdesc="XML-RPC client library for KDE"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://projects.kde.org/projects/kde/pim/kxmlrpcclient"
license="BSD-2-Clause"
depends_dev="
	ki18n-dev
	kio-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kxmlrpcclient-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
dd9a9c5aeda5631272188f591073c7d106b93be616c2826ba7e5febaec973077625a30f5874a4de0ac130216015a934f0450a07388bf11721ed845b5975c4682  kxmlrpcclient-5.91.0.tar.xz
"
