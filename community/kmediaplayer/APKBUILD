# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kmediaplayer
pkgver=5.91.0
pkgrel=0
pkgdesc="Media player framework for KDE 5"
# riscv64 disabled due to missing rust in recursive dependency
arch="all !armhf !s390x !riscv64" # armhf blocked by extra-cmake-modules and s390x blocked by polkit
url="https://community.kde.org/Frameworks"
license="X11 AND LGPL-2.1-or-later"
depends_dev="qt5-qtbase-dev kparts-dev kxmlgui-dev"
makedepends="$depends_dev extra-cmake-modules"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kmediaplayer-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# viewtest requires X11 to be running
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="
4574c33419f3ac76d4ba2d101a20974d0cfe095ec2c100cfb55903a1f19c04d4f68aa0ab93b9a549a999f749250425825b41af2e963db4c2bd4a65cb72d1efb4  kmediaplayer-5.91.0.tar.xz
"
