# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdesu
pkgver=5.91.0
pkgrel=0
pkgdesc="Integration with su for elevated privileges"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only"
depends_dev="
	kcoreaddons-dev
	ki18n-dev
	kpty-dev
	kservice-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdesu-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
# Since the goal of this library is to elevate privileges, suid being needed should be obvious
options="suid"

[ "$CARCH" = "ppc64le" ] && options="$options !check" # kdesutest is broken

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
5ebabc1bd43275ea77e7fded707191dc5887426626f486e600cd6513bf0dd80ced4632d44dcb0cda35f39cbb5000285f49508fcdabae365fe95ecfcb0ec5c253  kdesu-5.91.0.tar.xz
"
