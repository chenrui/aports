# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-unidecode
_pkgname=Unidecode
pkgver=1.3.3
pkgrel=0
pkgdesc="Python3 ASCII transliterations of Unicode text"
url="https://pypi.python.org/pypi/Unidecode"
arch="noarch"
license="GPL-2.0-or-later"
depends="python3"
makedepends="py3-setuptools"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	python3 setup.py build
}

check() {
	python3 setup.py test
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"

	# Add version suffix to executable files.
	local path; for path in "$pkgdir"/usr/bin/*; do
		mv "$path" "$path"-3
	done

	mkdir -p "$pkgdir"/usr/bin
	ln -s unidecode-3 "$pkgdir"/usr/bin/unidecode
}

sha512sums="
1c42f2530fa68c56ded097a92b580a3b1c223c957711778b81dd92c8543068a76a87379db7fbe4d2dcb6a9a6c2fc2aef28ed950e0c07d411ae2891c00a8ffea7  Unidecode-1.3.3.tar.gz
"
