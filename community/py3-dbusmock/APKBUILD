# Maintainer: Luca Weiss <luca@z3ntu.xyz>
pkgname=py3-dbusmock
_pyname=python-dbusmock
pkgver=0.26.1
pkgrel=0
pkgdesc="Mock D-Bus objects for tests"
url="https://github.com/martinpitt/python-dbusmock"
arch="noarch"
license="LGPL-3.0-or-later"
depends="py3-dbus py3-gobject3"
makedepends="python3-dev py3-setuptools"
checkdepends="dbus py3-nose bash"
_pypiprefix="${_pyname%${_pyname#?}}"
source="https://files.pythonhosted.org/packages/source/$_pypiprefix/$_pyname/$_pyname-$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	python3 setup.py build
}

check() {
	# Skip the fakeroot tests as they can't run under our fakeroot environment
	# https://github.com/martinpitt/python-dbusmock/issues/46
	env -u LD_PRELOAD python3 -m unittest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
dcb46f6dbd17471e2135c0d8fb63cbaecbc9445ae7bf74691abb03b93d5283ab41ea71071ece03870aaac19eaedd546f01c0839116421047f1ac77619f2c73f3  python-dbusmock-0.26.1.tar.gz
"
