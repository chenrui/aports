# Maintainer: Luca Weiss <luca@z3ntu.xyz>
pkgname=umockdev
pkgver=0.17.7
pkgrel=0
pkgdesc="Mock hardware devices for creating unit tests and bug reporting"
arch="all"
url="https://github.com/martinpitt/umockdev"
license="LGPL-2.1-or-later"
makedepends="eudev-dev gtk-doc meson vala libpcap-dev gobject-introspection-dev"
checkdepends="gphoto2 libgudev-dev py3-gobject3 usbutils xz"
if [ "$CARCH" != "ppc64le" ]; then
	checkdepends="$checkdepends evtest"
fi
options="!check" # fail on builders for some reason, works on CI and locally (and for upstream)
source="https://github.com/martinpitt/umockdev/archive/$pkgver/umockdev-$pkgver.tar.gz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	abuild-meson \
		-Dgtk_doc=true \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
6636d60e6c3f92409b169b5b55b61f099ccbe39484c07a4f63658e96b0730df6e8d98cabbc4b5dbab7953e7b3b6334977a49296e9136641078029a8923bca3b3  umockdev-0.17.7.tar.gz
"
