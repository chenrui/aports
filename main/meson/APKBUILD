# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=meson
pkgver=0.61.2
pkgrel=0
pkgdesc="Fast and user friendly build system"
url="https://mesonbuild.com"
arch="noarch"
license="Apache-2.0"
depends="samurai python3"
makedepends="py3-setuptools"
subpackages="
	$pkgname-doc
	$pkgname-vim::noarch
	$pkgname-bash-completion
	$pkgname-zsh-completion
	"
source="https://github.com/mesonbuild/meson/releases/download/$pkgver/meson-$pkgver.tar.gz
	abuild-meson
	"

build() {
	python3 setup.py build
}

check() {
	python3 setup.py check
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"

	install -Dm644 data/shell-completions/zsh/* -t "$pkgdir"/usr/share/zsh/site-functions
	install -Dm644 data/shell-completions/bash/* -t "$pkgdir"/usr/share/bash-completion/completions

	install -Dm0755 "$srcdir"/abuild-meson -t "$pkgdir"/usr/bin
}

vim() {
	pkgdesc="$pkgdesc (vim support)"
	install_if="vim $pkgname=$pkgver-r$pkgrel"
	cd "$builddir"
	for kind in ftdetect ftplugin indent syntax
	do
		mkdir -p "$subpkgdir"/usr/share/vim/vimfiles/$kind
		install -Dm644 \
			"$builddir"/data/syntax-highlighting/vim/$kind/meson.vim \
			"$subpkgdir"/usr/share/vim/vimfiles/$kind/meson.vim
	done
}

sha512sums="
0cbc686b23a4d0b74e723a97869898c1e44977ee354ed9186ccac3e527c00b7407f62a15435cb9588e9d63f4e87de54da0a7fac5b251079d1a13851f17d61529  meson-0.61.2.tar.gz
1029e7a7af86ff4a5636f8cea4b5dbe970a58741d2f25dad50712664f1e387078e7583fafd4f7d1d420c9e116b0ea72b20cf22532cfece9a7d2d8cf60c9aadab  abuild-meson
"
